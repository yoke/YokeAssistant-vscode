'use strict';

// imports
import * as vscode from 'vscode';

// the status class
export class Status implements vscode.Disposable {

    // the run button
    private readonly _runButton = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 4.0);

    // the debug button
    private readonly _debugButton = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 3.9);

    // is visible?
    private _visible: boolean = true;

    // the constructor
    constructor() {

        // init run button
        this._runButton.command = 'yoke.onRun';
        this._runButton.text = `$(triangle-right)`;
        this._runButton.tooltip = "Run the given target";

        // init debug button
        this._debugButton.command = 'yoke.onDebug';
        this._debugButton.text = `$(bug)`;
        this._debugButton.tooltip = "Debug the given target";

        // update visibility
        this.updateVisibility();
    }

    // dispose all objects
    public dispose() {

        for (const item of [
        this._runButton,
        this._debugButton,]) {
            item.dispose();
        }
    }

    // update visibility
    private updateVisibility() {
        for (const item of [
        this._runButton,
        this._debugButton,]) {
            if (this.visible && !!item.text) {
                item.show();
            } else {
                item.hide();
            }
        }
    }

    // get visible
    public get visible(): boolean {
        return this._visible;
    }

    // set visible  
    public set visible(v: boolean) {
        this._visible = v;
        this.updateVisibility();
    }

}