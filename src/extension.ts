'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { Assistant } from './assistant';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export async function activate(context: vscode.ExtensionContext) {

    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    console.log('Congratulations, your extension "yoke-assistant-debug" is now active!');

    // this extension is activated!
    console.log('xmake-vscode: actived!');

    // init xmake plugin
    const yoke = new Assistant(context);
    context.subscriptions.push(yoke);

    // register all commands of the xmake plugin
    function register(name: any, fn: any) {
        fn = fn.bind(yoke);
        return vscode.commands.registerCommand(name, _ => fn());
    }

    context.subscriptions.push(register('yoke.onRun', yoke.onRun));
    context.subscriptions.push(register('yoke.onDebug' , yoke.onDebug));

    // start xmake plugin
    await yoke.start();
}

// this method is called when your extension is deactivated
export function deactivate() {
}